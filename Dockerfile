FROM python:3.7-alpine

COPY requirements.txt .

# RUN pip3 install -y pyinstaller

# RUN pyinstaller 

COPY src/* /bin/

COPY data/ /data/

COPY README.md .

RUN pip install -r requirements.txt