#!/usr/bin/env python3

#
# @author Arnaud Ceol
#
# Load a dataset in a cbioportal docker instance
#
import sys, os, re, docker, subprocess
import click, configparser, logging

import datetime, time

from load_function import process_study

@click.command(
    context_settings = dict( help_option_names = ['-h', '--help'] )
)
@click.option('--root-folder', '-r',required=True,  help="Folder in which the studies to load are stored, e.g. /study/tmp/.")
@click.option('--study-folder', '-s',required=True,  help="Name of the folder to load.")
@click.option('--container-id', '-c',required=True,  help="ID of the container to use. This should be a running container")
@click.option('--mail', '-m',required=True,  help="email to which sending the report")
@click.option('--env',          '-e', required=False, help="Environment, optional")


def main(root_folder, study_folder, container_id, mail, env):
    click.echo("INFO: executing %s" % __file__)
    try:
        client = docker.from_env()
    except:
        click.echo("ERROR: failed to execute docker.from_env()")
        return

    click.echo("INFO: %s: client.containers.get(%s)" % (__file__, container_id))
    container = client.containers.get(container_id)

    process_study(root_folder, study_folder, root_folder, container, container_id, mail)



if __name__ == '__main__':
    main()

