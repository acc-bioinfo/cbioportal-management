#!/usr/bin/env python3
#
# @author Igor Pozdnyakov
import os, re, docker, subprocess
import click, configparser, logging

import datetime, time, sys

def process_study(root_folder, study_folder, report_folder, container, container_id, mail):

    # from https://stackoverflow.com/a/13891070
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d-%H%M%S')

    click.echo("INFO: %s Loading folder %s, timestamp: %s, container: %s" % (__file__, study_folder,timestamp,container_id))

    # Validate dataset
    # click.echo(container.exec_run("validateData.py --help"))

    click.echo("INFO: Start validation for %s" % study_folder)
    validate_command = "validateData.py -s %s%s -n" % (root_folder, study_folder)
    exit_code, output = container.exec_run(validate_command)

    if exit_code != 3 and exit_code != 0:
        # click.echo(exit_code)
        click.echo(output)
        click.echo("No data wil be loaded")
        return 1

    # sometimes cbioportalImporter.py -c remove-study call returns error code 137
    # this happens when docker runs out of memory. On my Mac I increas docker memory from 2GB to 4 BG and the problem went away
    # # Remove previous version
    click.echo("Remove dataset if it exists")
    rm_command = "cbioportalImporter.py -c remove-study  -meta  %s/%s/meta_study.txt" % (root_folder, study_folder)
    click.echo("INFO: executing %s" % rm_command)
    exit_code, output = container.exec_run(rm_command)

    # if exit_code == 3:
    #     click.echo("The dataset was not in the database. Skip removing.")
    if exit_code != 0:
        click.echo("ERROR: cbioportalImporter.py returned error code %s"% exit_code)
        if output:
            click.echo("ERROR: Output: %s" % output.decode("utf-8"))
        click.echo(output)
        return 1

    # # load new version
    click.echo("INFO: Start loading the dataset %s" % study_folder)
    load_command = "metaImport.py -n -s %s/%s --html=%s/report_%s.html -v -o" % (root_folder, study_folder, report_folder, timestamp)
    click.echo("executing %s" % load_command)

    start_time = time.time()
    exit_code, output = container.exec_run(load_command)
    end_time = time.time()
    click.echo("metaImport.py command execution took  %g seconds" % (end_time - start_time))

    if exit_code != 0:
        click.echo("metaImport.py exit_code: %s" % exit_code)
        if output:
            click.echo("metaImport.py output %s:" % output.decode("utf-8"))

    # reload cbioportal
    click.echo("Restarting the container")
    container.restart()

    # send mail
    click.echo("Send mail")
    mail_command = ['sh', '-c', 'echo "cBioPortal dataset %s has been successfully loaded" |  mailx -s "cBioPortal dataset upload report: %s" -A %s/report_%s.html %s' % (study_folder, study_folder, report_folder, timestamp, mail)]
    click.echo("Send mail to  %s" % mail)
    exit_code, output = container.exec_run(mail_command)

    click.echo("Study %s processed" % study_folder)

    return 0