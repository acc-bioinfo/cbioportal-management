#!/bin/sh

for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   

    case "$KEY" in
            location_input)              location_input=${VALUE} ;;
            location_output)    location_output=${VALUE} ;;     
            *)   
    esac    

done

echo "location_input = $location_input"
echo "location_output = $location_output"