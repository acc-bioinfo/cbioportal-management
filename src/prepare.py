#!/usr/bin/env python3
import os, glob, re, logging, datetime
import click
import collections

@click.command(
    context_settings = dict( help_option_names = ['-h', '--help'] )
)

@click.option('--type',         '-t',required=True, help="type_of_cancer value in meta_study output file.")
@click.option('--identifier',   '-i',required=True, help="cancer_study_identifier value in meta_study output file.")
@click.option('--name',         '-n',required=True, help="name value in meta_study output file.")
@click.option('--short_name',   '-sn',required=True,help="short_name value in meta_study output file.")
@click.option('--description',  '-d',required=True, help="description value in meta_study output file.")
@click.option('--groups',       '-g',required=True, help="groups value in meta_study output file.")

# @click.option('--meta-study', '-s', type=click.File('r'),required=True,  help="Path to the meta study file. Example: \n\
# type_of_cancer: brca \n\
# cancer_study_identifier: horizon_test_2018 \n\
# name: horizon (TEST 2018)\n\
# short_name: horizon (TEST)\n\
# description:horizon samples for testing \n\
# add_global_case_list: true ")

@click.option('--maf-dir',      '-m', type=click.Path('r'),required=True, help='Folder that contains one maf file for each sample to include in the the dataset.')
@click.option('--template-dir', '-t', type=click.Path('r'),required=True, help='Template folder (inculded with this project)')
@click.option('--output-dir',   '-o', type=click.Path(exists=False),required=True, help='Path where the dataset folder will be created (will be created if does not exist)')
@click.option('--env',          '-e', required=False, help="Environment, optional")

def main(type, identifier, name, short_name, description, groups, maf_dir, template_dir, output_dir, env):

    # print(type, identifier, name, short_name, description, groups, maf_dir, template_dir, output_dir)

    # if env == 'development': #
    #     tar = 'gtar'
    # else:
    #     tar = 'tar'

    dict_name_data_map = collections.OrderedDict({
        'type_of_cancer'            : type,
        'cancer_study_identifier'   : identifier,
        'name'                      : name,
        'short_name'                : short_name,
        'description'               : description,
        'groups'                    : groups,
        'add_global_case_list'      : True
    })

    #command-line submitted data verification goes here if we have to do  it later

    dict_meta_study = {}
    for keyword in dict_name_data_map:
        dict_meta_study[keyword] = dict_name_data_map[keyword]

    datetimestamp = datetime.datetime.today().strftime('%Y%m%d-%H%M%S')
    identifier_timestamp = "%s_%s" % (dict_meta_study['cancer_study_identifier'], datetimestamp)
    output_dir_root = output_dir
    output_dir = os.path.join(output_dir, identifier_timestamp)
    # Path should not already exists
    if os.path.isdir(output_dir):
        click.echo("Error: path %s already exists" % output_dir)
        return 1

    # Create output dir
    try:
        os.makedirs(output_dir)
    except:
        error = "failed to create  directory %s" % output_dir
        click.error(error)
        return 1

    # try:
    #     data_input_meta_study_file = meta_study.read().splitlines()
    #     dict_meta_study = dict(map(lambda line : map(str.strip, line.split(':')), data_input_meta_study_file))
    # except:
    #     click.echo("Cannot read %s " % meta_study)
    #     return 1

    try:
        os.makedirs(os.path.join(output_dir, 'case_lists'))
    except:
        error = "failed to create  directory %s" % 'case_lists'
        click.error(error)
        return 1

    # if 'cancer_study_identifier' not in dict_meta_study:
    #     click.echo("Cannot find cancer_study_identifier in %s " % meta_study)
    #     return

    dir_case_list = os.path.join(output_dir, dict_meta_study['cancer_study_identifier'], datetimestamp, 'case_lists')

    clinical_template_file = os.path.join(template_dir, 'data_clinical_sample.txt')
    with open(clinical_template_file, 'r') as fh:
        data_clinical_template = fh.read().splitlines()
        data_clinical_template = [line for line in data_clinical_template if line] #get rid of empty lines in templat

    files_maf = [f for f in glob.glob('%s/*' % maf_dir) if re.match('^.*\.maf$', f, flags=re.IGNORECASE)]
    if len(files_maf) < 1:
            click.echo("Did not find any maf files in input directory %s" % maf_dir)
            return

    data_clinical_output = data_clinical_template #headers

    data_file = {}
    data_merged = []
    patient_ids = []
    sample_ids  = []
    for file_maf in files_maf:
        basename = os.path.splitext(os.path.basename(file_maf))[0]
        m = re.search(r'((\w{10})-\w{2,7})', basename)
        if m and m.group(0) and m.group(1) and m.group(2):
            patient_id = m.group(2)
            sample_id  = m.group(1)
        else:
            patient_id = basename
            sample_id  = basename
        patient_ids.append(patient_id)
        sample_ids.append(sample_id)
        data_clinical_output.append("%s\t%s" % (patient_id, sample_id))

        with open(file_maf, 'r') as fh:
            data_file[file_maf] = fh.read().splitlines()
            data_merged.append(data_file[file_maf][2:])

    data_merged = [item for sublist in data_merged for item in sublist]
    data_merged = data_file[files_maf[0]][0:2] + data_merged


    # Create clinical data file
    filebase_data_clinical_sample_output = 'data_clinical_sample.txt'
    file_data_clinical_sample_output = os.path.join(output_dir, filebase_data_clinical_sample_output)
    with open(file_data_clinical_sample_output, 'w') as fh:
        fh.writelines('\n'.join(data_clinical_output) + '\n')

    click.echo("created %s" % file_data_clinical_sample_output)

    # update meta_study file (validator requires its presence)
    # dict_to_update = {
    #     'cancer_study_identifier' : dict_meta_study['cancer_study_identifier'],
    # }
    dict_to_update = dict_meta_study

    if not updateFile(template_dir, output_dir, 'meta_study.txt', dict_to_update):
        return 1

    # update meta_clinical_sample file
    dict_to_update = {
        'cancer_study_identifier' : dict_meta_study['cancer_study_identifier'],
        'data_filename'           : filebase_data_clinical_sample_output
    }

    if not updateFile(template_dir, output_dir, 'meta_clinical_sample.txt', dict_to_update):
        return 1

    # write out merged maf file
    # datetimestamp = datetime.datetime.today().strftime('%Y%m%d-%H%M%S')
    # filebase_data_mutation_extended_output = 'data_mutation_extended-%s.txt' % datetimestamp
    filebase_data_mutation_extended_output = 'data_mutation_extended.txt'

    file_data_mutation_extended_output = os.path.join(output_dir, filebase_data_mutation_extended_output)
    with open(file_data_mutation_extended_output, 'w') as fh:
        fh.writelines('\n'.join(data_merged) + '\n')

    click.echo("created %s" % file_data_mutation_extended_output)

    # update meta_mutations_extended file
    dict_to_update = {
        'cancer_study_identifier' : dict_meta_study['cancer_study_identifier'],
        'data_filename'           : filebase_data_mutation_extended_output
    }

    if not updateFile(template_dir, output_dir,'meta_mutations_extended.txt', dict_to_update):
      return 1

    # update case_list/cases_sequenced
    dict_to_update = {
        'cancer_study_identifier' : dict_meta_study['cancer_study_identifier'],
        'stable_id'               : "%s_sequenced" % dict_meta_study['cancer_study_identifier'],
        'case_list_description'   : {'replace': {'<Number of Samples>': str(len(sample_ids))}},
        'case_list_ids'           : '\t'.join(sample_ids)
    }

    if not updateFile(os.path.join(template_dir, 'case_lists'), os.path.join(output_dir, 'case_lists'), 'cases_sequenced.txt', dict_to_update):
        return 1

    #zip output_dir
    # zip_command = "%s czf %s.tgz %s --remove-files" % (tar, identifier_timestamp, output_dir)

    import subprocess, shutil
    # zip_command = [tar, 'czf', '%s/%s.tgz' % (output_dir_root, identifier_timestamp), '-C', output_dir, '.', '--remove-files']
    zipped_file_name = '%s/%s.tgz' % (output_dir_root, identifier_timestamp)

    import tarfile
    def make_tarfile(output_filename, source_dir):
        with tarfile.open(output_filename, "w:gz") as tar:
            tar.add(source_dir, arcname=os.path.basename(source_dir))

    make_tarfile(zipped_file_name, output_dir)
    if os.path.exists(zipped_file_name):
        click.echo("tarzipped file: %s " % zipped_file_name)
        # os.rename(zipped_file_name, "%s/%s" % (output_dir_root, zipped_file_name))
        # tar --remove-files flag does not always work, there are several tar flavors
        shutil.rmtree(output_dir, ignore_errors=True)

def updateFile(template_dir, target_dir, filename, dict_to_update):
    # filepath_template = os.path.join(templates_folder, filename)
    template_file = os.path.join(template_dir, filename)
    target_file = os.path.join(target_dir, filename)

    #filepath_template = os.path.join(self.config['general']['templates_folder'], filename)
    dict_data = collections.OrderedDict()
    list_data_template = []
    try:
        with open(template_file, 'r') as fh:
            list_template_lines = fh.read().splitlines()
            list_template_lines = [line for line in list_template_lines if line] #get rid of empty lines in template
    except:
        click.echo("Cannot read template %s " % template_file)
        return

    dict_data = collections.OrderedDict(map(lambda line : map(str.strip, line.split(':')), list_template_lines))
    for x in dict_to_update:
        if isinstance(dict_to_update[x], dict):
            for y in dict_to_update[x]:
                if y == 'replace':
                    if isinstance(dict_to_update[x][y], dict):
                        for z in dict_to_update[x][y]:
                            dict_data[x] = dict_data[x].replace(z, dict_to_update[x][y][z])
        else :
            dict_data[x] = dict_to_update[x]

    curpath = os.path.abspath(os.curdir)

    with open(target_file, 'w+') as fh:
        fh.writelines('\n'.join(list(["%s:%s" % (x, dict_data[x]) for x in dict_data]))  + '\n')

    click.echo("Created %s" % target_file)
    return target_dir


if __name__ == '__main__':
    main()