#!/usr/bin/env python3

#
# @author Igor Pozdnyakov
#
# Load a dataset in a cbioportal docker instance
#
# testing: from user home:
"""
mkdir source_cbioportal; mkdir target_bioportal
cd ~/cbioportal-management/Dataset/sample_data/horizon;
tar -czvf ../../../../source_cbioportal/test1.tgz *; tar -czvf ../../../../source_cbioportal/test2.tgz *; tar -czvf ../../../../source_cbioportal/test3.tgz *;

to start cbioportal on mac the path to "data" folder should be in one of the dirs where docker on mac
has permisssions to mount to, e.g. /Users

  docker run -d --restart=always \
  --name=cbioDB \
  --net=cbio-net \
  -e MYSQL_ROOT_PASSWORD='P@ssword1' \
  -e MYSQL_USER=cbio \
  -e MYSQL_PASSWORD='P@ssword1' \
  -e MYSQL_DATABASE=cbioportal \
  -v /Users/igorpozdnyakov/data/:/var/lib/mysql/ \
  -v /Users/igorpozdnyakov/cgds.sql:/docker-entrypoint-initdb.d/cgds.sql:ro \
  -v /Users/igorpozdnyakov/Downloads/seed-cbioportal_hg19_v2.7.3.sql.gz:/docker-entrypoint-initdb.d/seed_part1.sql.gz:ro \
  mysql:5.7

prepare test:
assuming this volume mapping:
    volumes:
    - /mnt/cbio-upload/study:/study/
    - /mnt/cbio-upload/logs:/tmp/cbioportal/
1. clean up:
rm -rf /mnt/cbio-upload/study/target/*
2. create several test archives from "horizon" data:
cd cbioportal/Dataset/sample_data/horizon
tar -czvf /mnt/cbio-upload/study/source/test1.tgz *; tar -czvf /mnt/cbio-upload/study/source/test2.tgz *; tar -czvf /mnt/cbio-upload/study/source/test3.tgz *;
3. run script
python load_datasets.py --upload-folder /mnt/cbio-upload/study/source --studies-folder /mnt/cbio-upload/study/target --mail igor.pozdnyakov@ieo.it --container-id 3ecf1cdb5012

# java errors with suggestion to run migrate_db.py might be fixed with this executed from where the docker-compose is run: cbioportal-oncokb-compose directory
docker-compose exec cbioportal migrate_db.py --properties-file src/main/resources/portal.properties --sql db-scripts/src/main/resources/migration.sql


"""

import sys, os, re, docker, shutil, subprocess
import click, configparser
import datetime, time, tarfile
from load_function import process_study

@click.command(
    context_settings = dict( help_option_names = ['-h', '--help'] )
)
@click.option('--upload-folder',  '-u', required=True,  help="Name of the folder where new datasets are uploaded.")
@click.option('--studies-folder', '-s', required=True,  help="Name of the folder where new datasets are uploaded.")
@click.option('--container-id',   '-c', required=True,  help="ID of the cbioportal container to load dataset into. This should be a running container")
@click.option('--mail',           '-m', required=True,  help="email where to send the report")
@click.option('--env',            '-e', required=False, help="Environment, optional")

# docker run -it --name cbioportal --volume /Users/igorpozdnyakov:/study -d bbb020676fac

def main(upload_folder, studies_folder, container_id, mail, env):

    errors = []

    if not os.path.isdir(upload_folder):
        errors.append("upload directory not found %s" % upload_folder)

    if not os.path.isdir(studies_folder):
        errors.append("studies directory not found %s" % studies_folder)

    client = docker.from_env()

    try:
        container = client.containers.get(container_id)
        click.echo("Container %s found" % container_id)
    except:
        errors.append("Container %s not found" % container_id)
        
    if errors:
        for error in errors:
            click.echo("ERROR: %s" % error)
        click.echo("Exit program.")
        sys.exit(1)
    
    # Validate the dataset
    try:
        container.exec_run("validateData.py --help")
    except:
        click.echo(sys.exc_info()[0])
        sys.exit(1)

    files_uploaded_zipped = []
    for file in os.listdir(upload_folder):
        if file.endswith(".tgz"):
            click.echo("File to decompress: %s" % file)
            files_uploaded_zipped.append(file)

    if not files_uploaded_zipped:
        error = "no files to unzip in %s" % upload_folder
        click.echo(error)
        sys.exit(0)

    files_moved_zipped = []
    for file in os.listdir(studies_folder):
        if file.endswith(".tgz"):
            click.echo("File moved: %s" % file)
            files_moved_zipped.append(file)


    unprocessed_uploads = list(set(files_uploaded_zipped) - set(files_moved_zipped))
    if not unprocessed_uploads:
        click.echo("INFO: no NEW archive files in %s" % (upload_folder))
        sys.exit(0)

    dir_unzipped_temp = os.path.join(studies_folder, 'tmp/')

    if not os.path.isdir(dir_unzipped_temp):
        try:
            click.echo("Create dir : %s" % dir_unzipped_temp)
            os.makedirs(dir_unzipped_temp)
        except:
            error = "ERROR: failed to create directory %s" % dir_unzipped_temp
            click.echo(error)
            sys.exit(1)

    dir_failed = os.path.join(studies_folder, 'fail')
    if not os.path.isdir(dir_failed):
        try:
            click.echo("Create dir : %s" % dir_failed)
            os.makedirs(dir_failed)
        except:
            error = "ERROR: failed to create directory %s" % dir_failed
            click.echo(error)
            sys.exit(1)


    archives_moved = []
    for file in unprocessed_uploads:
        filename_source_fullpath = os.path.join(upload_folder, file)
        filename_target_fullpath = os.path.join(studies_folder, file)
        try:
            shutil.copy(filename_source_fullpath, filename_target_fullpath)
            os.remove(filename_source_fullpath)
            archives_moved.append(filename_target_fullpath)
        except:
            error = "ERROR: failed to move file %s to %s" % (filename_source_fullpath, filename_target_fullpath)
            click.echo(error)
            continue

    message = "INFO: moved %s archives" % len(archives_moved)
    click.echo(message)

    studies_subfolder = studies_folder.split('/')[-1]
    pattern_studies_folder = r'^%s/' % studies_folder

    for file in archives_moved:
        message = "\n======INFO: processing archive %s======" % file
        click.echo(message)
        list_match = re.findall(r'([-\w]+)\.', file)
        if not list_match:
            message = "ERROR: failed to get filename from %s" % file
            click.echo(message)
            continue

        study_folder = list_match[0]

        tar = tarfile.open(file)
        tar.extractall(path=dir_unzipped_temp)
        tar.close()
        click.echo("%s has been extracted to %s" % (file,dir_unzipped_temp))

        single_study_subfolder = re.sub(pattern_studies_folder, '', study_folder)
        status = load_folder(study_folder, dir_failed, container, container_id, mail)

        # Remove temporary folder:
        try:
            shutil.rmtree("%s/%s" % (dir_unzipped_temp, study_folder))
        except:
            error = "ERROR: failed to remove temporary folder  %s/%s" % (dir_unzipped_temp, study_folder)
            click.echo(error)
            continue


def load_folder(study_folder, dir_failed, container, container_id, mail):

    # from https://stackoverflow.com/a/13891070
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d-%H%M%S')

    single_study_path = os.path.join(study_folder)
    click.echo("Loading folder %s, timestamp: %s, container: %s" % (single_study_path, timestamp, container_id))

    # Validate dataset
    # click.echo(container.exec_run("validateData.py --help"))

    click.echo("Start loading %s" % single_study_path)

    # load_command = "./scripts/load_single_dataset.py -r %s -s %s -c cbioportal --mail %s" % ("/study/tmp/", filename, mail)
    result = process_study("/study/tmp/", study_folder, "/study/report/", container, container_id, mail)
    # click.echo("executing a call to  %s" % load_command)
    # output_code = os.system(load_command)
    # click.echo("Process study status:  %s" % result)

    #TODO: handle fail; move datasets to /fail directory (a new command-line argument?)
   
    return result

if __name__ == '__main__':
    main()

